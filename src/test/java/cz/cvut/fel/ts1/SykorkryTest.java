package cz.cvut.fel.ts1;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SykorkryTest {

    @Test
    public void factorialTest(){
        Sykorkry sykorkry = new Sykorkry();
        assertEquals(120,sykorkry.factorial(5));
    }
}
